package spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Color implements ApplicationContextAware {

    private String name;

    public Color(String name) {
        this.name = name;
        System.out.println("1.Color 实例已创建");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("2.Color 属性赋值");
    }

    public void init() {
        System.out.println("3.Color 已初始化");
    }

    public void destroy() {
        System.out.println("4.Color 已销毁");
    }

    @Override
    public String toString() {
        return "Color{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("Color ApplicationContext: " + applicationContext);
    }
}
