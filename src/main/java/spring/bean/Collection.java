package spring.bean;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Collection {

    private String[] strArray;

    private List<String> strList;

    private Map<String, String> strMap;

    private Set<String> strSet;

    public String[] getStrArray() {
        return strArray;
    }

    public void setStrArray(String[] strArray) {
        this.strArray = strArray;
    }

    public List<String> getStrList() {
        return strList;
    }

    public void setStrList(List<String> strList) {
        this.strList = strList;
    }

    public Map<String, String> getStrMap() {
        return strMap;
    }

    public void setStrMap(Map<String, String> strMap) {
        this.strMap = strMap;
    }

    public Set<String> getStrSet() {
        return strSet;
    }

    public void setStrSet(Set<String> strSet) {
        this.strSet = strSet;
    }

    @Override
    public String toString() {
        return "Collection{" +
                "strArray=" + Arrays.toString(strArray) +
                ", strList=" + strList +
                ", strMap=" + strMap +
                ", strSet=" + strSet +
                '}';
    }
}
