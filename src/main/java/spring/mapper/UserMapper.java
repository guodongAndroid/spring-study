package spring.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import spring.bean.User;

import java.util.List;

public interface UserMapper {

    @Select("SELECT * FROM users WHERE id = #{id}")
    User getUserById(@Param("id") long id);

    @Select("SELECT * FROM users WHERE name = #{name}")
    List<User> getUserByName(@Param("name") String name);
}
