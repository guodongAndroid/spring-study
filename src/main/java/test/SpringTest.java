package test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.AppConfig;
import spring.bean.DataSource;
import spring.bean.User;
import spring.service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SpringTest {

    public static void main(String[] args) {
//        xmlIoc();
        annotationIoc();
    }

    private static void annotationIoc() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.getEnvironment().setActiveProfiles("test");

        context.register(AppConfig.class);
        context.refresh();

        System.out.println("IoC容器创建完成: " + context);

        /*User user = context.getBean(User.class);
        user.setName("1");
        System.out.println(user.getName());
        System.out.println(user.getAge());

        DataSource dataSource = context.getBean(DataSource.class);
        System.out.println(dataSource);*/

        /*UserService userService = context.getBean(UserService.class);

        User register = userService.register("10086", "10086", "10089@qq.com");
        System.out.println(register);

        User userById = userService.getUserById(register.getId());
        System.out.println("userById: " + userById);

        userById = userService.getUserByIdWithMapper(register.getId());
        System.out.println("userByIdWithMapper: " + userById);

        User userByEmail = userService.getUserByEmail(register.getEmail());
        System.out.println("userByEmail: " + userByEmail);

        register.setName(register.getName() + "-Modify");
        userService.updateUser(register);

        List<User> userByNames = userService.getUserByName(register.getName());
        System.out.println("userByName: " + Arrays.toString(userByNames.toArray()));

        userByNames = userService.getUserByNameWithMapper(register.getName());
        System.out.println("userByNameWithMapper: " + Arrays.toString(userByNames.toArray()));

        int deleteUser = userService.deleteUser(register.getId());
        System.out.println("Delete user(" + register.getId() + ") result: " + (deleteUser == 1));*/

        Map<String, User> users = context.getBeansOfType(User.class);
        System.out.println(users.get("bill").getAddress());
        System.out.println(users);

        context.close();
    }

    private static void xmlIoc() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        System.out.println("IoC容器创建完成: " + context);

        /*User user = context.getBean("user", User.class);
        System.out.println(user);*/

        /*Collection collection = context.getBean(Collection.class);
        System.out.println(collection);*/

        /*Object bean1 = context.getBean("myFactoryBean", Color.class);
        Object bean2 = context.getBean(Color.class);
        System.out.println(bean1);
        System.out.println(bean2);
        System.out.println(bean1 == bean2);*/

        /*Color color = context.getBean(Color.class);
        System.out.println(color);*/

        User user = context.getBean(User.class);
        System.out.println(user);

        DataSource dataSource = context.getBean(DataSource.class);
        System.out.println(dataSource);

        context.close();
    }

}
